# Seppiko Chart

![GitLab tag](https://img.shields.io/gitlab/v/tag/seppiko/chart?style=flat-square)

Seppiko Chart is a barcode microservice for Code39 \ Code93 \ Code128 \ ITF \ PDF417 \ DataMatrix \ Seppiko QRCode.
QRCode and Decode API.

Seppiko Chart is almost OOTB (out-of-the-box) software,you can easily build and run.

## Features

1. One dimensional code \\ QR Code and Datamatrix has text on image bottom(Only the POST request can be closed).
2. You can use [Zxing][zxing] api generator [Seppiko QRCode][sqrcode] QR code image.
3. Decode support can use image URL in addition to Base64 code and URL.
4. You can get text content with QR Code.
5. All barcode support add text on the bottom.But except for PDF417 and [Seppiko QRCode][sqrcode] can not add text with SVG.
6. All barcode support SVG,except for PDF417.
7. [Seppiko QRCode][sqrcode] support custom color.
8. You can use JMX manager this microservice.

## How to run

1. Install [OpenJDK 21][jdk] \\ [Git][git] and [Maven][mvn]
2. `git clone https://gitlab.com/seppiko/chart.git`
3. `cd chart`
4. `mvn spring-boot:run`
5. Enjoy it.

## API Document
See [https://seppiko.org/chart/doc/](https://seppiko.org/chart/doc/)

## Work with Docker
See [https://spring.io/guides/gs/spring-boot-docker/](https://spring.io/guides/gs/spring-boot-docker/)

[sb2]: https://spring.io/projects/spring-boot
[zxing]: https://github.com/zxing/zxing
[sqrcode]: https://github.com/seppiko/qrcode
[jdk]: https://adoptium.net/
[git]: https://git-scm.com/
[mvn]: https://maven.apache.org/
[batik]: https://xmlgraphics.apache.org/batik/

## Full Post JSON and default value

### Generator

```json
{
  "data": "",
  "encoding": "",
  "errorCorrectionLevel": "",
  "format": "",
  "needBase64": false,
  "enableJson": false,
  "width": -1,
  "height": -1,
  "margin": -1,
  "backgroundColor": "",
  "color": "",
  "needText": false,
  "size": -1,
  "type": -1
}
```

1. Format support `PNG` `JPEG` `GIF` `TEXT` `SVG` (PDF417 unsupported `TEXT` and `SVG`, Datamatrix unsupported `SVG`)
2. `errorCorrectionLevel` must be `L` `M` `Q` and `H`
3. D_Project (\qrd) `margin` between 0 and 31, `size` between 1 and 4, `type` between 0 and 9.
4. Only QRCode supported RGB color
5. ITF only support number
6. PNG unsupported transparent images

### Decode

```json
{
  "type": "",
  "data": ""
}
```

1. type support `base64` `uri` and `url`
2. if type is `base64` data is image `base64` like `iVBO...`
3. if type is `uri` data is like `data:image/png;base64,iVBO...`
4. if type is `url` data is barcode url

## Acknowledgments

1. [Spring boot 2][sb2]
2. [ZXing][zxing]
3. [Seppiko QRCode][sqrcode]
4. [Apache Batik][batik]

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate

## License

This project is released under [Apache License version 2.0](http://www.apache.org/licenses/)

## Sponsors

<a href="https://www.jetbrains.com/" target="_blank"><img src="https://seppiko.org/images/jetbrains.png" alt="JetBrians" width="100px"></a>
