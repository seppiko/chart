# Seppiko Chart

## 2.3.1 2024-03-11
1. Fixed bug

## 2.3.0 2024-03-09
1. Update Seppiko Commons Utils 2.11.0
2. Update Seppiko Commons Logging 3.2.0
3. Update Spring Boot 3.2.3
4. Update Google Zxing 3.5.3
5. Update Apache Batik 1.17
6. Update Kotlin 1.9.23
7. Update d_project module to Seppiko QRCode 0.2.0
8. Delete module
9. Update copyright
10. Update document

## 2.2.0 2023-01-04
1. Update Spring boot 3.0.1
2. Add `JPEG` compression
3. Update document
4. Update copyright
5. Update Seppiko Commons Utils 2.7.0

## 2.1.0 2022-12-03
1. Fixed bug
2. Update Seppiko Commons Utils 2.6.1
3. Update Spring boot 3.0.0
4. Update Google Zxing 3.5.1
5. Update Apache batik 1.16
6. Update Seppiko Commons Logging 3.0.0
7. Update Kotlin 1.7.22

## 2.0.0 2022-04-09
1. Update module
2. Update Java 17
3. Update Spring boot 2.6.6
4. Update Seppiko Commons Util 2.2.1
5. Add Seppiko Commons Logging 1.1.0.RELEASE
6. Rewrite with Kotlin

## 1.8.RELEASE 2021-12-22
1. Update Spring boot 2.6.2
2. Remove GLF
3. Fixed bug
4. Remove CORS

## 1.7.RELEASE 20211104

1. Update Spring Boot 2.5.5
2. Update Seppiko Commons Utils 1.5.0.RELEASE
3. Change TEXT newline
4. Optimize performance
5. Add CORS and health

## 1.6.RELEASE 20210515

1. Fixed bug
2. Update Seppiko Commons Utils 1.0.5.RELEASE
3. Update Spring Boot 2.4.5
4. Update Batik 1.14
5. Update Copyright

## 1.5.RELEASE 20201115

1. Update Spring boot to 2.4.0
2. Update Zxing to 3.4.1
3. Update Batik to 1.13

## 1.4.RELEASE 20201103

1. Add actuator module.Support JMX
2. Change color is string
3. Change Gson to Jackson
4. Add Seppiko Commons Util

## 1.3.RELEASE 20201015

1. Update StringUtil and MatrixController
2. Update Jetty to Undertow
3. Change SLF4J to LoggingManager for Abstract Logging implementation
4. Add GLF and spring-boot-starter-glf

## 1.2.RELEASE 20201004

1. fixed bug
2. Update HttpClientUtil
3. Change Jackson to Gson

## 1.1-RELEASE 20200922

1. Fixed Json NullPointException(add gson)
2. Change Color and BackgroundColor is String
3. When Color is BW qrd use original generator
4. Fixed qrd size bug
5. Add bottom text for QR Code and Datamatrix,except for qrd 

## 1.0-RELEASE 20200920

1. Redesigned code architecture
2. Add Image with bottom text
3. qrd support custom color
