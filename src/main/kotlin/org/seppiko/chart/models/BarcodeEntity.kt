/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart.models

/**
 *
 * @author Leonard Woo
 */
data class BarcodeEntity(
  var data: String = "",
  var encoding: String = "",
  var errorCorrectionLevel: String = "",
  var format: String = "",
  var needBase64: Boolean = false,
  var enableJson: Boolean = false,
  var width: Int = -1,
  var height: Int = -1,
  var margin: Int = -1,
  var backgroundColor: String = "",
  var color: String = "",
  var needText: Boolean = false,

  // Only D_Project
  var size: Int = -1,
  var type: Int = -1,
)
