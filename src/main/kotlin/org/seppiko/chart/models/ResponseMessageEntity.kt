/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart.models

import org.seppiko.chart.utils.ResponseUtil
import org.springframework.http.ResponseEntity

/**
 *
 * @author Leonard Woo
 */
data class ResponseMessageEntity(
 var code: Int,
 var message: String,
)

object ResponseMessage {

  fun badRequest(code: Int, message: String): ResponseEntity<ByteArray> {
    return sendJsonResp(400, code, message)
  }

  fun serverError(code: Int, message: String): ResponseEntity<ByteArray> {
    return sendJsonResp(500, code, message)
  }

  fun sendJsonResp(status: Int, code: Int, message: String): ResponseEntity<ByteArray> {
    return ResponseUtil.sendJsonResponse(
      status,
      ResponseMessageEntity(code, message)
    )
  }
}
