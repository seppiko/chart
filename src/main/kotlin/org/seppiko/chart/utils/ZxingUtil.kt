/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart.utils

import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.encoder.ByteMatrix
import org.seppiko.commons.logging.Logging
import org.seppiko.commons.logging.LoggingFactory
import org.seppiko.commons.utils.CharUtil
import java.lang.StringBuilder

/**
 * Zxing utility
 *
 * @author Leonard Woo
 */
object ZxingUtil {
  private val logger: Logging = LoggingFactory.getLogging(ZxingUtil::class.qualifiedName!!)

  private val CRLF: String = "" + CharUtil.CARRIAGE_RETURN + CharUtil.LINE_FEED

  fun convertByteArrays(bas: Array<ByteArray>, nl: String? = "" + CRLF): String {
    val sb = StringBuilder()
    for (i in bas.indices) {
      for (j in bas[0].indices) {
        sb.append(bas[i][j].toInt())
      }
      sb.append(nl)
    }
    return sb.toString()
  }

  fun convertBooleanArray(data: BooleanArray): String {
    val sb = StringBuilder()
    for (i in data.indices) {
      sb.append(if (data[i]) "1" else "0")
    }
    return sb.toString()
  }

  // see com.google.zxing.oned.OneDimensionalCodeWriter.renderResult
  fun renderResult(code: BooleanArray, width: Int, height: Int, margin: Int): BitMatrix {
    val inputWidth = code.size
    val fullWidth = inputWidth + margin
    val outputWidth = width.coerceAtLeast(fullWidth)
    val outputHeight = 1.coerceAtLeast(height)
    val multiple = outputWidth / fullWidth
    val leftPadding = (outputWidth - inputWidth * multiple) / 2

    val output = BitMatrix(outputWidth, outputHeight)

    var inputX = 0
    var outputX = leftPadding
    while (inputX < inputWidth) {
      if (code[inputX]) {
        output.setRegion(outputX, 0, multiple, outputHeight)
      }
      ++inputX
      outputX += multiple
    }

    return output
  }

  fun toBitMatrix(matrix: ByteMatrix, flag: Byte, height: Int, width: Int, margin: Int): BitMatrix {
    val inputWidth = matrix.width
    val inputHeight = matrix.height
    val qrWidth = inputWidth + margin * 2
    val qrHeight = inputHeight + margin * 2
    val outputWidth = width.coerceAtLeast(qrWidth)
    val outputHeight = height.coerceAtLeast(qrHeight)
    val multiple = (outputWidth / qrWidth).coerceAtMost(outputHeight / qrHeight)
    return toBitMatrix(matrix, flag, outputHeight, outputWidth, multiple, multiple)
  }

  fun toBitMatrix(matrix: ByteMatrix, flag: Byte, height: Int, width: Int, cellHeight: Int,
                  cellWidth: Int): BitMatrix {
    val inputWidth = matrix.width
    val inputHeight = matrix.height
    val leftPadding = (width - inputWidth * cellWidth) / 2
    val topPadding = (height - inputHeight * cellHeight) / 2
    val output = BitMatrix(width, height)
    var inputY = 0
    var outputY = topPadding
    while (inputY < inputHeight) {
      var inputX = 0
      var outputX = leftPadding
      while (inputX < inputWidth) {
        if (matrix[inputX, inputY] == flag) {
          output.setRegion(outputX, outputY, cellWidth, cellHeight)
        }
        ++inputX
        outputX += cellWidth
      }
      ++inputY
      outputY += cellHeight
    }
    return output
  }
}