/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.seppiko.chart.utils

import com.d_project.qrcode.QRCode
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.encoder.ByteMatrix
import org.seppiko.chart.exceptions.SeppikoCheckException
import org.seppiko.chart.exceptions.SeppikoImageGeneratorException
import org.seppiko.chart.exceptions.SeppikoProcessorException
import org.seppiko.chart.models.BarcodeEntity
import org.seppiko.chart.utils.DProjectUtil.createGIFImage
import org.seppiko.commons.logging.Logging
import org.seppiko.commons.logging.LoggingFactory
import java.awt.*
import java.awt.image.BufferedImage
import java.io.*
import java.nio.charset.StandardCharsets


/**
 * Zxing image utility
 *
 * @author Leonard Woo
 */
object ZxingImageUtil {
  private val logger: Logging = LoggingFactory.getLogging(ZxingImageUtil::class.qualifiedName!!)

  const val BLACK = 0x000000
  const val WHITE = 0xFFFFFF

  val DEFAULT_FONT: Font = Font("Arial", Font.PLAIN, 18)

  @Throws(SeppikoCheckException::class, SeppikoProcessorException::class)
  fun imageGenerator(entity: BarcodeEntity, matrix: ByteMatrix): ByteArray? {
    val image = toBufferedImage(matrix, entity.width, entity.height, entity.margin,
      Integer.decode(entity.color), Integer.decode(entity.backgroundColor))
    return ImageUtil.writeImageBytes(entity, image)
  }

  @Throws(SeppikoCheckException::class, SeppikoProcessorException::class)
  fun imageGenerator(entity: BarcodeEntity, matrix: BitMatrix): ByteArray? {
    val image = toBufferedImage(matrix,
      Integer.decode(entity.color), Integer.decode(entity.backgroundColor))
    return ImageUtil.writeImageBytes(entity, image)
  }

  @Throws(SeppikoCheckException::class, SeppikoProcessorException::class)
  fun imageGenerator(entity: BarcodeEntity, matrix: BooleanArray): ByteArray? {
    val image = toBufferedImage(matrix, entity.width, entity.height, entity.margin,
      Integer.decode(entity.color), Integer.decode(entity.backgroundColor))
    return ImageUtil.writeImageBytes(entity, image)
  }

  @Throws(SeppikoCheckException::class, SeppikoImageGeneratorException::class)
  fun imageGenerator(entity: BarcodeEntity, qrcode: QRCode): ByteArray? {
    try {
      ByteArrayOutputStream().use { out ->
        return if ("gif" == entity.format.lowercase()) {
          createGIFImage(qrcode, entity.size, entity.margin).write(out)
          out.toByteArray()
        } else {
          val image: BufferedImage = qrcode.createImage(entity.size, entity.margin)
          ImageUtil.writeImageBytes(entity, image)
        }
      }
    } catch (e: IOException) {
      logger.warn("Image generator failed.", e)
      throw SeppikoImageGeneratorException()
    }
  }

  @Throws(SeppikoImageGeneratorException::class)
  fun svgGenerator(entity: BarcodeEntity, matrix: ByteMatrix?): ByteArray? {
    try {
      ByteArrayOutputStream().use { out ->
        val content: String? = if (entity.needText) entity.data else null
        SvgImageUtil.toSVGDocument(matrix!!, content,
          OutputStreamWriter(BufferedOutputStream(out), StandardCharsets.ISO_8859_1.name()),
          entity.width, entity.height, entity.margin,
          Integer.decode(entity.color),
          Integer.decode(entity.backgroundColor)
        )
        return out.toByteArray()
      }
    } catch (ex: IOException) {
      throw SeppikoImageGeneratorException(ex.message, ex)
    }
  }

  @Throws(SeppikoImageGeneratorException::class)
  fun svgGenerator(entity: BarcodeEntity, matrix: BooleanArray?): ByteArray? {
    try {
      ByteArrayOutputStream().use { out ->
        val content: String? = if (entity.needText) entity.data else null
        SvgImageUtil.toSVGDocument(
          matrix!!,
          OutputStreamWriter(
            BufferedOutputStream(out),
            StandardCharsets.ISO_8859_1.name()
          ),
          content,
          entity.width,
          entity.height,
          entity.margin,
          Integer.decode(entity.color),
          Integer.decode(entity.backgroundColor)
        )
        return out.toByteArray()
      }
    } catch (ex: IOException) {
      throw SeppikoImageGeneratorException(ex.message, ex)
    }
  }

  /*
   * Note:
   * That the input matrix uses 0 == white, 1 == black, while the output
   * matrix uses 0 == black, 255 == white (i.e. an 8 bit greyscale bitmap).
   */
  private fun toBufferedImage(input: ByteMatrix, width: Int, height: Int, margin: Int, color: Int,
                      backgroundColor: Int): BufferedImage {
    return toBufferedImage(ZxingUtil.toBitMatrix(input, 1.toByte(), height, width, margin), backgroundColor, color)
  }

  private fun toBufferedImage(code: BooleanArray, width: Int, height: Int, margin: Int, backgroundColor: Int,
                      color: Int): BufferedImage {
    return toBufferedImage(ZxingUtil.renderResult(code, width, height, margin), backgroundColor, color)
  }

  private fun toBufferedImage(matrix: BitMatrix, backgroundColor: Int, color: Int): BufferedImage {
    val width = matrix.width
    val height = matrix.height

    var imgType = BufferedImage.TYPE_BYTE_GRAY
    if (color != BLACK && backgroundColor != WHITE) {
      imgType = BufferedImage.TYPE_INT_RGB
    }

    val image = BufferedImage(width, height, imgType)
    for (x in 0 until width) {
      for (y in 0 until height) {
        image.setRGB(x, y, if (matrix[x, y]) color else backgroundColor)
      }
    }
    return image
  }



}