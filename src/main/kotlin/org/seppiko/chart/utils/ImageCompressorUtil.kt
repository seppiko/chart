/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.image.ImageUtil;

/**
 * @author Leonard Woo
 */
object ImageCompressorUtil {

  private val logger: Logging = LoggingFactory.getLogging(ImageCompressorUtil::class.qualifiedName)

  fun compress(image: ByteArray?, format: String): ByteArray? {
    try {
      if (image == null || image.isEmpty()) {
        throw NullPointerException("Image byte array must be not NULL.");
      }
      val bufImg: BufferedImage? = ImageIO.read(ByteArrayInputStream(image));
      if ("jpg" == format || "jpeg" == format) {
        return ImageUtil.compressor(bufImg, "jpeg", 0.5f, false);
//      } else if ("png".equals(format)) {
//        return pngCompress(bufImg);
      }
    } catch (ex: IOException) {
      logger.error(ex.message)
    } catch(ex: RuntimeException) {
      logger.error(ex.message)
    }
    return image
  }
}
