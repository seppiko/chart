/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.seppiko.chart.utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonNode
import org.seppiko.commons.logging.Logging
import org.seppiko.commons.logging.LoggingFactory

/**
 * JSON utility with Jackson
 *
 * @author Leonard Woo
 */
object JsonUtil {
  private val logger: Logging = LoggingFactory.getLogging(JsonUtil::class.qualifiedName!!)

  private val mapper = ObjectMapper()
  private val NULL_JSONNODE = mapper.createObjectNode()

  init {
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
    mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
    mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    mapper.enable(DeserializationFeature.USE_LONG_FOR_INTS)
    mapper.enable(DeserializationFeature.USE_BIG_INTEGER_FOR_INTS)
    mapper.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS)
  }

  fun <T> toJson(t: T): String {
    try {
      return mapper.writeValueAsString(t)
    } catch (e: JsonProcessingException) {
      logger.warn("Json string generator exception.", e)
    }
    return "{}"
  }

  fun <T> fromJson(json: String?, type: Class<T>?): T? {
    try {
      return mapper.readValue(json, type)
    } catch (e: JsonProcessingException) {
      logger.warn("Json string parse exception.", e)
    }
    return null
  }

  fun fromJsonObject(json: String?): JsonNode? {
    try {
      return mapper.reader().readTree(json)
    } catch (e: JsonProcessingException) {
      logger.warn("Json string parse exception.", e)
    }
    return NULL_JSONNODE
  }
}