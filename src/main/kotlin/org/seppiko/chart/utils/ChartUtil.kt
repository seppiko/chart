/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart.utils

import org.seppiko.chart.configures.ChartConfigure
import org.seppiko.chart.exceptions.SeppikoImageFormatException
import org.seppiko.commons.utils.StringUtil
import org.seppiko.commons.utils.http.HttpClientUtil
import org.seppiko.commons.utils.http.HttpHeaders
import org.seppiko.commons.utils.http.HttpMethod
import org.springframework.http.MediaType
import java.io.InputStream
import java.nio.charset.StandardCharsets
import java.util.*

/**
 * Seppiko Chart utility
 *
 * @author Leonard Woo
 */
object ChartUtil {

  @Throws(SeppikoImageFormatException::class)
  fun format(format: String): MediaType {
    if ("png" == format.lowercase()) {
      return MediaType.IMAGE_PNG;
    }
    if ("gif" == format.lowercase()) {
      return MediaType.IMAGE_GIF;
    }
    if ("jpg" == format.lowercase() || "jpeg" == format.lowercase()) {
      return MediaType.IMAGE_JPEG;
    }
    throw SeppikoImageFormatException("$format is not supported");
  }

  fun convertUtf8ToLatin1(data: String): String {
    return StringUtil.transcoding(data, StandardCharsets.UTF_8, StandardCharsets.ISO_8859_1)
  }

  fun requestImage(url: String): InputStream? {
    if (StringUtil.isNullOrEmpty(url)) {
      throw IllegalArgumentException()
    }
    val header: HttpHeaders = HttpHeaders.newHeaders()
    header.addHeaders("Accept", MediaType.ALL_VALUE)
    header.addHeaders("Cache-Control", "no-cache")
    header.addHeaders("Pragma", "no-cache")
    header.addHeaders("User-Agent", "Seppiko Chart/" + ChartConfigure.getVersionString())

    val req = HttpClientUtil.getRequest(url, HttpMethod.GET, 20, header, "")
    val resp = HttpClientUtil.getResponseInputStream(req, null, null)
    return resp.body()
  }

  fun isText(format: String): Boolean {
    return format.lowercase().matches("te?xt".toRegex())
  }

  fun isSvg(format: String): Boolean {
    return format.lowercase() == "svg"
  }
}