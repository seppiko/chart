/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart.utils

import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import java.nio.charset.StandardCharsets

/**
 * Http Response utility
 *
 * @author Leonard Woo
 */
class ResponseUtil {
  companion object {

    fun <T> sendJsonResponse(status: Int, t: T): ResponseEntity<ByteArray> {
      return sendResponse(status,
        MediaType.APPLICATION_JSON,
        JsonUtil.toJson(t).toByteArray(StandardCharsets.UTF_8))
    }

    fun sendResponse(status: Int, type: MediaType, content: ByteArray): ResponseEntity<ByteArray> {
      return ResponseEntity.status(status)
        .contentType(type)
        .contentLength(content.size.toLong())
        .body(content)
    }

  }
}