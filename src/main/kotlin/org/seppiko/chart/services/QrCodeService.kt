/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart.services

import com.google.zxing.WriterException
import com.google.zxing.qrcode.encoder.ByteMatrix
import org.seppiko.chart.exceptions.SeppikoCheckException
import org.seppiko.chart.exceptions.SeppikoProcessorException
import org.seppiko.chart.models.BarcodeEntity
import org.seppiko.chart.models.ResponseMessageEntity
import org.seppiko.chart.models.ServiceEntity
import org.seppiko.chart.utils.*
import org.seppiko.chart.utils.ZxingCodeUtil
import org.seppiko.commons.utils.codec.Base64Util
import org.springframework.http.MediaType
import org.springframework.stereotype.Service

/**
 * QRCode service
 *
 * @author Leonard Woo
 */
@Service
class QrCodeService {

  /**
   * zxing qrcode
   */
  @Throws(WriterException::class, SeppikoCheckException::class, SeppikoProcessorException::class)
  fun qrCodeCHX(entity: BarcodeEntity): ServiceEntity {
    val matrix: ByteMatrix = ZxingCodeUtil.qrCode.encode(entity)
    return qrCodeCHX0(entity, matrix)
  }

  /**
   * d_project qrcode
   */
  @Throws(SeppikoCheckException::class, SeppikoProcessorException::class)
  fun qrCodeDCHX(entity: BarcodeEntity): ServiceEntity {

    entity.needText = false // Close bottom text

    val flag = (Integer.decode(entity.backgroundColor) == ZxingImageUtil.WHITE) &&
            (Integer.decode(entity.color) == ZxingImageUtil.BLACK)

    val qrCode = DProjectUtil.getQRCode( entity.data, entity.type,
      DProjectUtil.parseErrorCorrectionLevel(entity.errorCorrectionLevel))

    if (flag && !(ChartUtil.isText(entity.format) || ChartUtil.isSvg(entity.format)) ) {
      val se = ServiceEntity()
      se.type = ChartUtil.format(entity.format)
      if (!(entity.needText)) {
        se.data = ZxingImageUtil.imageGenerator(entity, qrCode)
        return se
      }
    }

    val matrix = DProjectUtil.convertToZxingByteMatrix(qrCode)
    entity.width = matrix.width
    entity.height = matrix.height
    return qrCodeCHX0(entity, matrix)
  }

  /**
   * d_project qrcode encode with zxing image generator
   */
  @Throws(SeppikoCheckException::class, SeppikoProcessorException::class)
  fun qrCodeXCHX(entity: BarcodeEntity): ServiceEntity {
    val qrCode = DProjectUtil.getQRCode( entity.data, 0,
      DProjectUtil.parseErrorCorrectionLevel(entity.errorCorrectionLevel))
    val matrix = DProjectUtil.convertToZxingByteMatrix(qrCode)
    return qrCodeCHX0(entity, matrix)
  }

  private fun qrCodeCHX0(entity: BarcodeEntity, matrix: ByteMatrix): ServiceEntity {
    val se = ServiceEntity()

    if (ChartUtil.isText(entity.format)) {
      se.type = MediaType.TEXT_PLAIN
      se.data = ZxingUtil.convertByteArrays(matrix.array).toByteArray()
    } else if (ChartUtil.isSvg(entity.format)) {
      se.type = MediaType.valueOf("image/svg+xml")
      se.data = ZxingImageUtil.svgGenerator(entity, matrix)
    } else {
      se.type = ChartUtil.format(entity.format)
      se.data = ZxingImageUtil.imageGenerator(entity, matrix)
    }

    if (entity.needBase64) {
      val base64 = Base64Util.encodeString(se.data)
      se.type = MediaType.TEXT_PLAIN
      se.data = base64.toByteArray()
    }

    if (entity.enableJson) {
      val json = JsonUtil.toJson(se.data?.let { String(it) }?.let { ResponseMessageEntity(200, it) })
      se.type = MediaType.APPLICATION_JSON
      se.data = json.toByteArray()
    }

    return se
  }

}