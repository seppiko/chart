/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart.services

import com.google.zxing.oned.Code128Writer
import com.google.zxing.oned.Code39Writer
import com.google.zxing.oned.Code93Writer
import com.google.zxing.oned.ITFWriter
import org.seppiko.chart.exceptions.SeppikoCheckException
import org.seppiko.chart.exceptions.SeppikoProcessorException
import org.seppiko.chart.models.BarcodeEntity
import org.seppiko.chart.models.ServiceEntity
import org.seppiko.chart.utils.ChartUtil
import org.seppiko.chart.utils.ZxingImageUtil
import org.seppiko.chart.utils.ZxingUtil
import org.springframework.http.MediaType
import org.springframework.stereotype.Service

/**
 * One-Dimensional service
 * >> include code39 code93 code128 and ITF
 *
 * @author Leonard Woo
 */
@Service
class OneDimensionalService {

  @Throws(SeppikoCheckException::class, SeppikoProcessorException::class)
  fun code39CHX(entity: BarcodeEntity): ServiceEntity {
    val matrix = Code39Writer().encode(entity.data)
    return onedCHX(entity, matrix)
  }

  @Throws(SeppikoCheckException::class, SeppikoProcessorException::class)
  fun code93CHX(entity: BarcodeEntity): ServiceEntity {
    val matrix = Code93Writer().encode(entity.data)
    return onedCHX(entity, matrix)
  }

  @Throws(SeppikoCheckException::class, SeppikoProcessorException::class)
  fun code128CHX(entity: BarcodeEntity): ServiceEntity {
    val matrix = Code128Writer().encode(entity.data)
    return onedCHX(entity, matrix)
  }

  @Throws(SeppikoCheckException::class, SeppikoProcessorException::class)
  fun itfCHX(entity: BarcodeEntity): ServiceEntity {
    val matrix = ITFWriter().encode(entity.data)
    return onedCHX(entity, matrix)
  }


  @Throws(SeppikoCheckException::class, SeppikoProcessorException::class)
  private fun onedCHX(entity: BarcodeEntity, matrix: BooleanArray): ServiceEntity {
    val se = ServiceEntity()

    entity.backgroundColor = ZxingImageUtil.WHITE.toString()
    entity.color = ZxingImageUtil.BLACK.toString()
    entity.margin = 5

    if (ChartUtil.isText(entity.format)) {
      se.type = MediaType.TEXT_PLAIN
      se.data = ZxingUtil.convertBooleanArray(matrix).toByteArray()
    } else if (ChartUtil.isSvg(entity.format)) {
      se.type = MediaType.valueOf("image/svg+xml")
      se.data = ZxingImageUtil.svgGenerator(entity, matrix)
    } else {
      se.type = ChartUtil.format(entity.format)
      se.data = ZxingImageUtil.imageGenerator(entity, matrix)
    }

    return se
  }
}