/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart.services

import com.google.zxing.WriterException
import org.seppiko.chart.exceptions.SeppikoCheckException
import org.seppiko.chart.exceptions.SeppikoProcessorException
import org.seppiko.chart.models.BarcodeEntity
import org.seppiko.chart.models.ResponseMessageEntity
import org.seppiko.chart.models.ServiceEntity
import org.seppiko.chart.utils.*
import org.seppiko.commons.utils.codec.Base64Util
import org.springframework.http.MediaType
import org.springframework.stereotype.Service

/**
 * Matrix code service (PDF417 and DataMatrix)
 *
 * @author Leonard Woo
 */
@Service
class MatrixCodeService {

  @Throws(WriterException::class, SeppikoCheckException::class, SeppikoProcessorException::class)
  fun pdf417CHX(entity: BarcodeEntity): ServiceEntity {
    val se = ServiceEntity()

    entity.backgroundColor = ZxingImageUtil.WHITE.toString()
    entity.color = ZxingImageUtil.BLACK.toString()
    val matrix = ZxingCodeUtil.pdf417.encode(entity)
    se.type = ChartUtil.format(entity.format)
    se.data = ZxingImageUtil.imageGenerator(entity, matrix)

    if (entity.needBase64) {
      val base64 = Base64Util.encodeString(se.data)
      se.type = MediaType.TEXT_PLAIN
      se.data = base64.toByteArray()
    }
    if (entity.enableJson) {
      val json = JsonUtil.toJson(se.data?.let { String(it) }?.let { ResponseMessageEntity(200, it) })
      se.type = MediaType.APPLICATION_JSON
      se.data = json.toByteArray()
    }

    return se
  }

  @Throws(WriterException::class, SeppikoCheckException::class, SeppikoProcessorException::class)
  fun datamatrixCHX(entity: BarcodeEntity): ServiceEntity {
    val se = ServiceEntity()

    entity.backgroundColor = ZxingImageUtil.WHITE.toString()
    entity.color = ZxingImageUtil.BLACK.toString()
    val matrix = ZxingCodeUtil.dataMatrix.encode(entity.data)
    if (ChartUtil.isSvg(entity.format)) {
      se.type = MediaType.valueOf("image/svg+xml")
      se.data = ZxingImageUtil.svgGenerator(entity, matrix)
    } else {
      se.type = ChartUtil.format(entity.format)
      se.data = ZxingImageUtil.imageGenerator(entity, matrix)
    }

    if (entity.needBase64) {
      val base64 = Base64Util.encodeString(se.data)
      se.type = MediaType.TEXT_PLAIN
      se.data = base64.toByteArray()
    }
    if (entity.enableJson) {
      val json = JsonUtil.toJson(se.data?.let { String(it) }?.let { ResponseMessageEntity(200, it) })
      se.type = MediaType.APPLICATION_JSON
      se.data = json.toByteArray()
    }

    return se
  }

}