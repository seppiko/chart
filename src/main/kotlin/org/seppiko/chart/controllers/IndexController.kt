/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart.controllers

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.view.json.MappingJackson2JsonView

/**
 *
 * @author Leonard Woo
 */
@Controller
class IndexController {

  @RequestMapping(value = ["/"])
  fun indexContentHandleExecution(model: ModelMap?): ModelAndView? {
    val modelAndView = ModelAndView()
    modelAndView.view = MappingJackson2JsonView()
    modelAndView.status = HttpStatus.BAD_REQUEST
    modelAndView.addObject("code", 400)
    modelAndView.addObject("message", "Bad request")
    return modelAndView
  }

}