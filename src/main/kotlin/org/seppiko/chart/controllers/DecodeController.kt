/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart.controllers

import org.seppiko.chart.exceptions.SeppikoProcessorException
import org.seppiko.chart.models.ResponseMessage
import org.seppiko.chart.services.DecodeService
import org.seppiko.chart.utils.ChartUtil
import org.seppiko.chart.utils.JsonUtil.fromJsonObject
import org.seppiko.chart.utils.ResponseUtil
import org.seppiko.commons.logging.Logging
import org.seppiko.commons.logging.LoggingFactory
import org.seppiko.commons.utils.StringUtil
import org.seppiko.commons.utils.codec.Base64Util
import org.seppiko.commons.utils.exceptions.HttpClientException
import org.seppiko.commons.utils.exceptions.HttpRuntimeException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.io.BufferedInputStream
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStream
import java.net.URI
import java.net.URISyntaxException

/**
 *
 * @author Leonard Woo
 */
@RestController
class DecodeController {
  private val logger: Logging = LoggingFactory.getLogging(DecodeController::class.qualifiedName!!)

  @Autowired
  private lateinit var service: DecodeService

  @GetMapping("/decode")
  fun decodeGetContentHandleExecution(): ResponseEntity<ByteArray> {
    return ResponseMessage.sendJsonResp(405, 405, "Method Not Allowed.Only POST")
  }

  @PostMapping("/decode")
  fun decodePostContentHandleExecution(@RequestBody body: String): ResponseEntity<ByteArray> {
    if (StringUtil.isNullOrEmpty(body)) {
      return ResponseMessage.sendJsonResp(400, 404, "Not found any request.")
    }

    val root = fromJsonObject(body)
      ?: return ResponseMessage.sendJsonResp(404, 404, "Not found any data.")
    val type = root["type"].asText("").lowercase()
    val data = root["data"].asText("")

    if (StringUtil.isNullOrEmpty(data)) {
      return ResponseMessage.sendJsonResp(400, 4000, "Data must not empty or null.")
    }

    val input: InputStream?
    if (type == "base64") {
      //iVBO..
      input = ByteArrayInputStream(Base64Util.decode(data))
    } else if(type == "uri") {
      //data:image/png;base64,iVBO...
      try {
        input = BufferedInputStream(URI(data).toURL().openStream())
      } catch (ex: IOException) {
        logger.warn("Image didn't exist", ex)
        return ResponseMessage.sendJsonResp(400, 404, "URI decode failed.")
      }
    } else if(type == "url") {
      //http://localhost/qr.png
      try {
        input = ChartUtil.requestImage(data)
      } catch (e: URISyntaxException) {
        return ResponseMessage.sendJsonResp(404, 4040, "Data is not URL syntax.")
      } catch (e: HttpClientException) {
        return ResponseMessage.sendJsonResp(404, 4041, "URL content acquisition failed.")
      } catch (e: HttpRuntimeException) {
        return ResponseMessage.sendJsonResp(404, 4042, "URL content acquisition failed.")
      } catch (e: InterruptedException) {
        return ResponseMessage.sendJsonResp(404, 4043, "URL content acquisition failed.")
      } catch (e: IllegalArgumentException) {
        return ResponseMessage.sendJsonResp(404, 4044, "URL must not be null or empty.")
      }
      if (input == null) {
        return ResponseMessage.sendJsonResp(404, 4049, "URL content acquisition failed.")
      }
    } else {
      return ResponseMessage.sendJsonResp(400, 400, "Unsupported type.")
    }

    return try {
      val se = service.decodeCHX(input)
      ResponseUtil.sendResponse(200, se.type, se.data!!)
    } catch (e: SeppikoProcessorException) {
      ResponseMessage.badRequest(e.code, e.message)
    }
  }

}