/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart.controllers

import org.seppiko.chart.configures.Environment
import org.seppiko.chart.exceptions.SeppikoCheckException
import org.seppiko.chart.exceptions.SeppikoProcessorException
import org.seppiko.chart.models.BarcodeEntity
import org.seppiko.chart.models.ResponseMessage
import org.seppiko.chart.services.MatrixCodeService
import org.seppiko.chart.utils.ChartUtil
import org.seppiko.chart.utils.JsonUtil
import org.seppiko.chart.utils.ResponseUtil
import org.seppiko.commons.logging.Logging
import org.seppiko.commons.logging.LoggingFactory
import org.seppiko.commons.utils.StringUtil
import org.seppiko.commons.utils.http.URLCodecUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.io.UnsupportedEncodingException

/**
 *
 * @author Leonard Woo
 */
@RestController
class MatrixController {
  private val logger: Logging = LoggingFactory.getLogging(MatrixController::class.qualifiedName!!)

  @Autowired
  private lateinit var service: MatrixCodeService

  @GetMapping("/datamatrix")
  fun datamatrixGetContentHandleExecution(
    @RequestParam(name = "data", required = false, defaultValue = Environment.DEFAULT_DATA_WORD) data: String,
    @RequestParam(value = "size", required = false, defaultValue = "128x128") size: String,
    @RequestParam(value = "format", required = false, defaultValue = "png") format: String,
  ): ResponseEntity<ByteArray> {
    val entity = BarcodeEntity()

    entity.data = ChartUtil.convertUtf8ToLatin1(URLCodecUtil.decode(data))

    val sizes = size.split("x".toRegex()).toTypedArray()
    entity.width = sizes[0].toInt()
    entity.height = sizes[1].toInt()

    entity.margin = 1

    val f = URLCodecUtil.decode(format).lowercase()
    if (f.contains('|')) {
      val fs = f.split("\\|".toRegex()).toTypedArray()
      entity.format = fs[0]
      entity.needBase64 = (fs[1] == "base64")
    } else if ("json" == f) {
      entity.format = "png"
      entity.needBase64 = true
      entity.enableJson = true
    } else {
      entity.format = f
    }

    return try {
      val se = service.datamatrixCHX(entity)
      if (entity.enableJson) {
        if (se.type == MediaType.TEXT_PLAIN) {
          return ResponseMessage.sendJsonResp(200, 200, se.data.contentToString())
        }
      }
      ResponseUtil.sendResponse(200, se.type, se.data!!)
    } catch (e: SeppikoCheckException) {
      logger.error(e.message, e)
      ResponseMessage.badRequest(501, e.message!!)
    } catch (e: SeppikoProcessorException) {
      val outMsg = "Create failed"
      logger.error(outMsg, e)
      ResponseMessage.badRequest(500, outMsg)
    }
  }

  @PostMapping("/datamatrix")
  fun datamatrixPostContentHandleExecution(@RequestBody body: String): ResponseEntity<ByteArray> {
    if (StringUtil.isNullOrEmpty(body)) {
      return ResponseMessage.sendJsonResp(400, 404, "Not found any request.")
    }

    val entity = JsonUtil.fromJson(body, BarcodeEntity::class.java)
      ?: return ResponseMessage.sendJsonResp(404, 404, "Not found any data.")

    return try {
      val se = service.datamatrixCHX(entity)
      if (entity.enableJson) {
        if (se.type == MediaType.TEXT_PLAIN) {
          return ResponseMessage.sendJsonResp(200, 200, se.data.contentToString())
        }
      }
      ResponseUtil.sendResponse(200, se.type, se.data!!)
    } catch (e: SeppikoCheckException) {
      logger.error(e.message, e)
      ResponseMessage.badRequest(501, e.message!!)
    } catch (e: SeppikoProcessorException) {
      val outMsg = "Create failed"
      logger.error(outMsg, e)
      ResponseMessage.badRequest(500, outMsg)
    }
  }

  @GetMapping("/pdf417")
  fun pdf417GetContentHandleExecution(
    @RequestParam(value = "data", required = false, defaultValue = Environment.DEFAULT_DATA_WORD) data: String,
    @RequestParam(value = "encoding", required = false, defaultValue = "ISO-8859-1") encoding: String,
    @RequestParam(value = "eclevel", required = false, defaultValue = "2") ecLevel: String,
    @RequestParam(value = "size", required = false, defaultValue = "300x100") size: String,
    @RequestParam(value = "margin", required = false, defaultValue = "5") margin: String,
    @RequestParam(value = "format", required = false, defaultValue = "png") format: String,
  ): ResponseEntity<ByteArray> {
    val entity = BarcodeEntity()

    entity.encoding = encoding
    try {
      entity.data = URLCodecUtil.decode(data, encoding)
    } catch (e: UnsupportedEncodingException) {
      entity.data = URLCodecUtil.decode(data, "UTF-8")
      entity.encoding = "UTF-8"
    }
    entity.errorCorrectionLevel = ecLevel

    val sizes = size.split("x".toRegex()).toTypedArray()
    entity.width = sizes[0].toInt()
    entity.height = sizes[1].toInt()

    entity.margin = margin.toInt()

    val f = URLCodecUtil.decode(format, "UTF-8").lowercase()
    if (f.contains('|')) {
      val fs = f.split("\\|".toRegex()).toTypedArray()
      entity.format = fs[0]
      entity.needBase64 = (fs[1] == "base64")
    } else if ("json" == f) {
      entity.format = "png"
      entity.needBase64 = true
      entity.enableJson = true
    } else {
      entity.format = f
    }

    return try {
      val se = service.pdf417CHX(entity)
      if (entity.enableJson) {
        if (se.type == MediaType.TEXT_PLAIN) {
          return ResponseMessage.sendJsonResp(200, 200, se.data.contentToString())
        }
      }
      ResponseUtil.sendResponse(200, se.type, se.data!!)
    } catch (e: SeppikoCheckException) {
      logger.error(e.message, e)
      ResponseMessage.badRequest(501, e.message!!)
    } catch (e: SeppikoProcessorException) {
      val outMsg = "Create failed"
      logger.error(outMsg, e)
      ResponseMessage.badRequest(500, outMsg)
    }
  }

  @PostMapping("/pdf417")
  fun pdf417PostContentHandleExecution(@RequestBody body: String): ResponseEntity<ByteArray> {
    if (StringUtil.isNullOrEmpty(body)) {
      return ResponseMessage.sendJsonResp(400, 404, "Not found any request.")
    }

    val entity = JsonUtil.fromJson(body, BarcodeEntity::class.java)
      ?: return ResponseMessage.sendJsonResp(404, 404, "Not found any data.")

    return try {
      val se = service.pdf417CHX(entity)
      if (entity.enableJson) {
        if (se.type == MediaType.TEXT_PLAIN) {
          return ResponseMessage.sendJsonResp(200, 200, se.data.contentToString())
        }
      }
      ResponseUtil.sendResponse(200, se.type, se.data!!)
    } catch (e: SeppikoCheckException) {
      logger.error(e.message, e)
      ResponseMessage.badRequest(501, e.message!!)
    } catch (e: SeppikoProcessorException) {
      val outMsg = "Create failed"
      logger.error(outMsg, e)
      ResponseMessage.badRequest(500, outMsg)
    }
  }
}