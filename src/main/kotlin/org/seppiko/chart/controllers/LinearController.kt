/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart.controllers

import org.seppiko.chart.configures.Environment
import org.seppiko.chart.exceptions.SeppikoCheckException
import org.seppiko.chart.exceptions.SeppikoProcessorException
import org.seppiko.chart.models.BarcodeEntity
import org.seppiko.chart.models.ResponseMessage
import org.seppiko.chart.services.OneDimensionalService
import org.seppiko.chart.utils.JsonUtil
import org.seppiko.chart.utils.ResponseUtil
import org.seppiko.commons.logging.Logging
import org.seppiko.commons.logging.LoggingFactory
import org.seppiko.commons.utils.StringUtil
import org.seppiko.commons.utils.http.URLCodecUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 *
 * @author Leonard Woo
 */
@RestController
class LinearController {
  private val logger: Logging = LoggingFactory.getLogging(LinearController::class.qualifiedName!!)

  @Autowired
  private lateinit var service: OneDimensionalService

  @GetMapping("/code39")
  fun code39GetContentHandleExecution(
    @RequestParam(name = "data", required = false, defaultValue = Environment.DEFAULT_DATA_WORD_NUMBER) data: String,
    @RequestParam(value = "size", required = false, defaultValue = "128x64") size: String,
    @RequestParam(value = "format", required = false, defaultValue = "png") format: String,
  ): ResponseEntity<ByteArray> {
    val entity = BarcodeEntity()

    entity.data = URLCodecUtil.decode(data)

    val sizes = size.split("x".toRegex()).toTypedArray()
    entity.width = sizes[0].toInt()
    entity.height = sizes[1].toInt()

    val f = URLCodecUtil.decode(format).lowercase()
    if (f.contains('|')) {
      val fs = f.split("\\|".toRegex()).toTypedArray()
      entity.format = fs[0]
      entity.needBase64 = (fs[1] == "base64")
    } else if ("json" == f) {
      entity.format = "png"
      entity.needBase64 = true
      entity.enableJson = true
    } else {
      entity.format = f
    }
    entity.needText = true

    return try {
      val se = service.code39CHX(entity)
      if (entity.enableJson) {
        if (se.type == MediaType.TEXT_PLAIN) {
          return ResponseMessage.sendJsonResp(200, 200, se.data.contentToString())
        }
      }
      ResponseUtil.sendResponse(200, se.type, se.data!!)
    } catch (e: SeppikoCheckException) {
      logger.error(e.message, e)
      ResponseMessage.badRequest(501, e.message!!)
    } catch (e: SeppikoProcessorException) {
      val outMsg = "Create failed"
      logger.error(outMsg, e)
      ResponseMessage.badRequest(500, outMsg)
    }
  }

  @PostMapping("/code39")
  fun code39PostContentHandleExecution(@RequestBody body: String): ResponseEntity<ByteArray> {
    if (StringUtil.isNullOrEmpty(body)) {
      return ResponseMessage.sendJsonResp(400, 404, "Not found any request.")
    }

    val entity = JsonUtil.fromJson(body, BarcodeEntity::class.java)
      ?: return ResponseMessage.sendJsonResp(404, 404, "Not found any data.")

    return try {
      val se = service.code39CHX(entity)
      if (entity.enableJson) {
        if (se.type == MediaType.TEXT_PLAIN) {
          return ResponseMessage.sendJsonResp(200, 200, se.data.contentToString())
        }
      }
      ResponseUtil.sendResponse(200, se.type, se.data!!)
    } catch (e: SeppikoCheckException) {
      logger.error(e.message, e)
      ResponseMessage.badRequest(501, e.message!!)
    } catch (e: SeppikoProcessorException) {
      val outMsg = "Create failed"
      logger.error(outMsg, e)
      ResponseMessage.badRequest(500, outMsg)
    }
  }

  @GetMapping("/code93")
  fun code93GetContentHandleExecution(
    @RequestParam(name = "data", required = false, defaultValue = Environment.DEFAULT_DATA_WORD_NUMBER) data: String,
    @RequestParam(value = "size", required = false, defaultValue = "128x64") size: String,
    @RequestParam(value = "format", required = false, defaultValue = "png") format: String,
  ): ResponseEntity<ByteArray> {
    val entity = BarcodeEntity()

    entity.data = URLCodecUtil.decode(data)

    val sizes = size.split("x".toRegex()).toTypedArray()
    entity.width = sizes[0].toInt()
    entity.height = sizes[1].toInt()

    val f = URLCodecUtil.decode(format).lowercase()
    if (f.contains('|')) {
      val fs = f.split("\\|".toRegex()).toTypedArray()
      entity.format = fs[0]
      entity.needBase64 = (fs[1] == "base64")
    } else if ("json" == f) {
      entity.format = "png"
      entity.needBase64 = true
      entity.enableJson = true
    } else {
      entity.format = f
    }
    entity.needText = true


    return try {
      val se = service.code93CHX(entity)
      if (entity.enableJson) {
        if (se.type == MediaType.TEXT_PLAIN) {
          return ResponseMessage.sendJsonResp(200, 200, se.data.contentToString())
        }
      }
      ResponseUtil.sendResponse(200, se.type, se.data!!)
    } catch (e: SeppikoCheckException) {
      logger.error(e.message, e)
      ResponseMessage.badRequest(501, e.message!!)
    } catch (e: SeppikoProcessorException) {
      val outMsg = "Create failed"
      logger.error(outMsg, e)
      ResponseMessage.badRequest(500, outMsg)
    }
  }

  @PostMapping("/code93")
  fun code93PostContentHandleExecution(@RequestBody body: String): ResponseEntity<ByteArray> {
    if (StringUtil.isNullOrEmpty(body)) {
      return ResponseMessage.sendJsonResp(400, 404, "Not found any request.")
    }

    val entity = JsonUtil.fromJson(body, BarcodeEntity::class.java)
      ?: return ResponseMessage.sendJsonResp(404, 404, "Not found any data.")

    return try {
      val se = service.code93CHX(entity)
      if (entity.enableJson) {
        if (se.type == MediaType.TEXT_PLAIN) {
          return ResponseMessage.sendJsonResp(200, 200, se.data.contentToString())
        }
      }
      ResponseUtil.sendResponse(200, se.type, se.data!!)
    } catch (e: SeppikoCheckException) {
      logger.error(e.message, e)
      ResponseMessage.badRequest(501, e.message!!)
    } catch (e: SeppikoProcessorException) {
      val outMsg = "Create failed"
      logger.error(outMsg, e)
      ResponseMessage.badRequest(500, outMsg)
    }
  }

  @GetMapping("/code128")
  fun code128GetContentHandleExecution(
    @RequestParam(name = "data", required = false, defaultValue = Environment.DEFAULT_DATA_WORD_NUMBER) data: String,
    @RequestParam(value = "size", required = false, defaultValue = "128x64") size: String,
    @RequestParam(value = "format", required = false, defaultValue = "png") format: String,
  ): ResponseEntity<ByteArray> {
    val entity = BarcodeEntity()

    entity.data = URLCodecUtil.decode(data)

    val sizes = size.split("x".toRegex()).toTypedArray()
    entity.width = sizes[0].toInt()
    entity.height = sizes[1].toInt()

    val f = URLCodecUtil.decode(format).lowercase()
    if (f.contains('|')) {
      val fs = f.split("\\|".toRegex()).toTypedArray()
      entity.format = fs[0]
      entity.needBase64 = (fs[1] == "base64")
    } else if ("json" == f) {
      entity.format = "png"
      entity.needBase64 = true
      entity.enableJson = true
    } else {
      entity.format = f
    }
    entity.needText = true

    return try {
      val se = service.code128CHX(entity)
      if (entity.enableJson) {
        if (se.type == MediaType.TEXT_PLAIN) {
          return ResponseMessage.sendJsonResp(200, 200, se.data.contentToString())
        }
      }
      ResponseUtil.sendResponse(200, se.type, se.data!!)
    } catch (e: SeppikoCheckException) {
      logger.error(e.message, e)
      ResponseMessage.badRequest(501, e.message!!)
    } catch (e: SeppikoProcessorException) {
      val outMsg = "Create failed"
      logger.error(outMsg, e)
      ResponseMessage.badRequest(500, outMsg)
    }
  }

  @PostMapping("/code128")
  fun code128PostContentHandleExecution(@RequestBody body: String): ResponseEntity<ByteArray> {
    if (StringUtil.isNullOrEmpty(body)) {
      return ResponseMessage.sendJsonResp(404, 404, "Not found any request.")
    }

    val entity = JsonUtil.fromJson(body, BarcodeEntity::class.java)
      ?: return ResponseMessage.sendJsonResp(404, 404, "Not found any data.")

    return try {
      val se = service.code128CHX(entity)
      if (entity.enableJson) {
        if (se.type == MediaType.TEXT_PLAIN) {
          return ResponseMessage.sendJsonResp(200, 200, se.data.contentToString())
        }
      }
      ResponseUtil.sendResponse(200, se.type, se.data!!)
    } catch (e: SeppikoCheckException) {
      logger.error(e.message, e)
      ResponseMessage.badRequest(501, e.message!!)
    } catch (e: SeppikoProcessorException) {
      val outMsg = "Create failed"
      logger.error(outMsg, e)
      ResponseMessage.badRequest(500, outMsg)
    }
  }

  @GetMapping("/itf")
  fun itfGetContentHandleExecution(
    @RequestParam(name = "data", required = false, defaultValue = Environment.DEFAULT_DATA_NUMBER) data: String,
    @RequestParam(value = "size", required = false, defaultValue = "128x64") size: String,
    @RequestParam(value = "format", required = false, defaultValue = "png") format: String,
  ): ResponseEntity<ByteArray> {
    val entity = BarcodeEntity()

    entity.data = URLCodecUtil.decode(data)

    val sizes = size.split("x".toRegex()).toTypedArray()
    entity.width = sizes[0].toInt()
    entity.height = sizes[1].toInt()

    val f = URLCodecUtil.decode(format).lowercase()
    if (f.contains('|')) {
      val fs = f.split("\\|".toRegex()).toTypedArray()
      entity.format = fs[0]
      entity.needBase64 = (fs[1] == "base64")
    } else if ("json" == f) {
      entity.format = "png"
      entity.needBase64 = true
      entity.enableJson = true
    } else {
      entity.format = f
    }
    entity.needText = true

    return try {
      val se = service.code128CHX(entity)
      if (entity.enableJson) {
        if (se.type == MediaType.TEXT_PLAIN) {
          return ResponseMessage.sendJsonResp(200, 200, se.data.contentToString())
        }
      }
      ResponseUtil.sendResponse(200, se.type, se.data!!)
    } catch (e: SeppikoCheckException) {
      logger.error(e.message, e)
      ResponseMessage.badRequest(501, e.message!!)
    } catch (e: SeppikoProcessorException) {
      val outMsg = "Create failed"
      logger.error(outMsg, e)
      ResponseMessage.badRequest(500, outMsg)
    }
  }

  @PostMapping("/itf")
  fun itfPostContentHandleExecution(@RequestBody body: String): ResponseEntity<ByteArray> {
    if (StringUtil.isNullOrEmpty(body)) {
      return ResponseMessage.sendJsonResp(404, 404, "Not found any request.")
    }

    val entity = JsonUtil.fromJson(body, BarcodeEntity::class.java)
      ?: return ResponseMessage.sendJsonResp(404, 404, "Not found any data.")

    return try {
      val se = service.itfCHX(entity)
      if (entity.enableJson) {
        if (se.type == MediaType.TEXT_PLAIN) {
          return ResponseMessage.sendJsonResp(200, 200, se.data.contentToString())
        }
      }
      ResponseUtil.sendResponse(200, se.type, se.data!!)
    } catch (e: SeppikoCheckException) {
      logger.error(e.message, e)
      ResponseMessage.badRequest(501, e.message!!)
    } catch (e: SeppikoProcessorException) {
      val outMsg = "Create failed"
      logger.error(outMsg, e)
      ResponseMessage.badRequest(500, outMsg)
    }
  }
}