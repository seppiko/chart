/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.chart;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;
import org.seppiko.chart.models.BarcodeEntity;
import org.seppiko.chart.utils.JsonUtil;
import org.seppiko.chart.utils.ZxingImageUtil;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author Leonard Woo
 */
@SpringBootTest
public class SeppikoTest {

  private static final Logging log = LoggingFactory.getLogging(SeppikoTest.class.getName());

  @Test
  public void test() {
    log.info("" + (false && !(true || true || false)));
  }

  @Test
  public void test1() {
    //    log.info(JsonUtil.toJson(new BarcodeEntity()));

    JsonNode root =
        JsonUtil.INSTANCE.fromJsonObject("{\"type\":\"url\",\"data\":\"http://localhost/qr.png\"}");
    String type = root.get("type").asText();
    String data = root.get("data").asText();
    log.info(">type " + type + " >data " + data);
  }
  
  @Test
  public void test2() {
    String json =
        "{"
            + "\"data\": \"seppiko\","
            + "\"encoding\": \"utf-8\","
            + "\"errorCorrectionLevel\": \"l\","
            + "\"format\": \"svg\","
            + "\"needBase64\": false,"
            + "\"enableJson\": false,"
            + "\"width\": 200,"
            + "\"height\": 200,"
            + "\"margin\": 1,"
            + "\"backgroundColor\": \"0xFAFAFA\","
            + "\"color\": \"0x0A0A0A\","
            + "\"needText\": true"
            + "}";

    BarcodeEntity entity = JsonUtil.INSTANCE.fromJson(json, BarcodeEntity.class);
    log.info(entity.toString());
  }

  @Test
  public void test3() {
    log.info(ZxingImageUtil.BLACK + "");
    log.info(ZxingImageUtil.WHITE + "");
    log.info(Integer.decode("0xFFFFFF") + "");
  }
}
